![Banner](img/portada-web.png)
# Titulo del proyecto
#### Gestor de Carceles

## indice
1. [Caracteristicas 📄](#caracteristicas)
2. [Contenido del proyecto](#contenido-del-proyecto)
3. [Tecnologías](#tecnologias)
4. [IDE](#ide)
5. [Instalación](#instalacion)
6. [Demo](#demo)
7. [Autor(es)](#autores)
8. [Institución Academica]()
9. [Referencias]()
***

#### Caracteristicas
-Uso de Css recomentado: [ver](https://gitlab.com/jefersonrr/curricullum-vitae/-/tree/master/css/1)
***
#### Contenido del Proyecto

| Archivo      | Descripción  |
|--------------|--------------|
| [index.jsp](https://gitlab.com/jefersonrr/gestorcarcerles/-/blob/master/web/index.jsp) | Archivo principal de invocación al Login de la pagina web.
| [abogado.jsp](https://gitlab.com/jefersonrr/gestorcarcerles/-/blob/master/web/abogado.jsp) | Archivo JS con el proceso de lectura del JSON y sus funciones adicionales para la impresión de resultados|
| [celdas.jsp](https://gitlab.com/jefersonrr/gestorcarcerles/-/blob/master/web/celdas.jsp) | Archivo JS con el proceso de lectura del JSON y sus funciones adicionales para la impresión de resultados|
| [fichaMedica.jsp](https://gitlab.com/jefersonrr/gestorcarcerles/-/blob/master/web/fichaMedica.jsp) | Archivo JS con el proceso de lectura del JSON y sus funciones adicionales para la impresión de resultados|
| [guardias.jsp](https://gitlab.com/jefersonrr/gestorcarcerles/-/blob/master/web/guardias.jsp) | Archivo JS con el proceso de lectura del JSON y sus funciones adicionales para la impresión de resultados|
| [modificarAbogado.jsp](https://gitlab.com/jefersonrr/gestorcarcerles/-/blob/master/web/ModificarAbogado.jsp) | Archivo JS con el proceso de lectura del JSON y sus funciones adicionales para la impresión de resultados|
| [modificarGuardia.jsp](https://gitlab.com/jefersonrr/gestorcarcerles/-/blob/master/web/modificarGuardia.jsp) | Archivo JS con el proceso de lectura del JSON y sus funciones adicionales para la impresión de resultados|
| [modificarRecluso.jsp](https://gitlab.com/jefersonrr/gestorcarcerles/-/blob/master/web/modificarRecluso.jsp) | Archivo JS con el proceso de lectura del JSON y sus funciones adicionales para la impresión de resultados|
| [modificarUsuario.jsp](https://gitlab.com/jefersonrr/gestorcarcerles/-/blob/master/web/modificarUsuario.jsp) | Archivo JS con el proceso de lectura del JSON y sus funciones adicionales para la impresión de resultados|
| [modificarsentencia.jsp](https://gitlab.com/jefersonrr/gestorcarcerles/-/blob/master/web/modificarsentencia.jsp) | Archivo JS con el proceso de lectura del JSON y sus funciones adicionales para la impresión de resultados|
| [nuevoPadecimiento.jsp](https://gitlab.com/jefersonrr/gestorcarcerles/-/blob/master/web/nuevoPadecimiento.jsp) | Archivo JS con el proceso de lectura del JSON y sus funciones adicionales para la impresión de resultados|
| [pabellones.jsp](https://gitlab.com/jefersonrr/gestorcarcerles/-/blob/master/web/pabellones.jsp) | Archivo JS con el proceso de lectura del JSON y sus funciones adicionales para la impresión de resultados|
| [reclusos.jsp](https://gitlab.com/jefersonrr/gestorcarcerles/-/blob/master/web/reclusos.jsp) | Archivo JS con el proceso de lectura del JSON y sus funciones adicionales para la impresión de resultados|
| [registrarAbogado.jsp](https://gitlab.com/jefersonrr/gestorcarcerles/-/blob/master/web/registrarAbogado.jsp) | Archivo JS con el proceso de lectura del JSON y sus funciones adicionales para la impresión de resultados|
| [registrarGuardia.jsp](https://gitlab.com/jefersonrr/gestorcarcerles/-/blob/master/web/registrarGuardia.jsp) | Archivo JS con el proceso de lectura del JSON y sus funciones adicionales para la impresión de resultados|
| [registrarUsuario.jsp](https://gitlab.com/jefersonrr/gestorcarcerles/-/blob/master/web/registrarUsuario.jsp) | Archivo JS con el proceso de lectura del JSON y sus funciones adicionales para la impresión de resultados|
| [registrarRecluso.jsp](https://gitlab.com/jefersonrr/gestorcarcerles/-/blob/master/web/registrarRecluso.jsp) | Archivo JS con el proceso de lectura del JSON y sus funciones adicionales para la impresión de resultados|
| [sentencias.jsp](https://gitlab.com/jefersonrr/gestorcarcerles/-/blob/master/web/sentencias.jsp) | Archivo JS con el proceso de lectura del JSON y sus funciones adicionales para la impresión de resultados|
| [usuarioAdmin.jsp](https://gitlab.com/jefersonrr/gestorcarcerles/-/blob/master/web/usuarioAdmin.jsp) | Archivo JS con el proceso de lectura del JSON y sus funciones adicionales para la impresión de resultados|
| [verAbogado.jsp](https://gitlab.com/jefersonrr/gestorcarcerles/-/blob/master/web/verAbogado.jsp) | Archivo JS con el proceso de lectura del JSON y sus funciones adicionales para la impresión de resultados|
| [verFichaMedica.jsp](https://gitlab.com/jefersonrr/gestorcarcerles/-/blob/master/web/verFichaMedica.jsp) | Archivo JS con el proceso de lectura del JSON y sus funciones adicionales para la impresión de resultados|
| [verGuardia.jsp](https://gitlab.com/jefersonrr/gestorcarcerles/-/blob/master/web/verGuardia.jsp) | Archivo JS con el proceso de lectura del JSON y sus funciones adicionales para la impresión de resultados|
| [verRecluso.jsp](https://gitlab.com/jefersonrr/gestorcarcerles/-/blob/master/web/verRecluso.jsp) | Archivo JS con el proceso de lectura del JSON y sus funciones adicionales para la impresión de resultados|
| [verSentencia.jsp](https://gitlab.com/jefersonrr/gestorcarcerles/-/blob/master/web/verSentencia.jsp) | Archivo JS con el proceso de lectura del JSON y sus funciones adicionales para la impresión de resultados|


- [index.html]
***
#### Tecnologias
-Uso de BOOTSTRAP : [ir](https://getbootstrap.com/)
***
#### IDE
***
#### Instalacion

1. Local
 - Descargar el repositorio ubicado en [Descarga]()
 - Abrir el archivo index.html desde el navegador predeterminado

 2. Gitlab
 - Realizado un fork.

 ***
#### Demo

- incluir la url de proyecto desplegada
#### Autor(es)

Realizado por [Jeferson Rodriguez](<jefersonrr@ufps.edu.co>)
#### Institucion Academica
Proyecto desarrollado en la materia de Bases de Datos de la carrera de [Ingeneria de Sistemas](https://ingsistemas.cloud.ufps.edu.co/) de la [Universidad Francisco de Paula Santander](https://ww2.ufps.edu.co/) 
